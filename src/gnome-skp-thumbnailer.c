/* -*- Mode: C; indent-tabs-mode: nil; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * This file is part of Gnome SketchUp Thumbnailer.
 * https://gitlab.gnome.org/glerro/gnome-skp-thumbnailer
 *
 * gnome-skp-thumbnailer.c
 *
 * Copyright (c) 2023 Gianni Lerro {glerro} ~ <glerro@pm.me>
 *
 * Gnome SketchUp Thumbnailer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gnome SkeychUp Thumbnailer is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Gnome SketchUp Thumbnailer. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2023 Gianni Lerro <glerro@pm.me>
 */

#include <stdlib.h>
#include <glib.h>
#include <gio/gio.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

/* Convert from BigEndian to host endianness */
static guint32
get_guint32 (const gchar *data)
{
    guint32 *ptr = (guint32 *) (data);
    return GUINT32_FROM_BE (ptr[0]);
}

/* Check if the input file is a valid skp file. */
gboolean
is_skpfile (GFileInputStream *input_file_stream,
            GError          **error)
{
    const guchar skpsignature[32] =
    {
        0xFF, 0xFE, 0xFF, 0x0E, 0x53, 0x00, 0x6B, 0x00, 0x65, 0x00, 0x74,
        /* .     .     .     .     S     .     k     .     e     .     t */
        0x00, 0x63, 0x00, 0x68, 0x00, 0x55, 0x00, 0x70, 0x00, 0x20, 0x00,
        /* .     c     .     h     .     U     .     p     .     .     . */
        0x4D, 0x00, 0x6F, 0x00, 0x64, 0x00, 0x65, 0x00, 0x6C, 0x00
        /* M     .     o     .     d     .     e     .     l     . */
    };

    guchar skpheader[32];

    if (g_input_stream_read (G_INPUT_STREAM (input_file_stream), &skpheader, sizeof (skpheader), NULL, error) < 0)
    {
        return FALSE;
    }

    if (memcmp (skpsignature, skpheader, sizeof(skpsignature)) != 0)
    {
        return FALSE;
    }

    return TRUE;
}

/* Get the version of the skp file and also return the length of the version string. */
guchar *
get_skp_version(GFileInputStream *input_file_stream,
                GError          **error)
{
    /* Offset of the Sketchup file version */
    const guint8 skp_version_offset = 0x24;

    guint count = 0, len = 0;
    static guchar skp_version[10];
    guchar tmp;

    if (g_seekable_seek (G_SEEKABLE (input_file_stream), skp_version_offset, G_SEEK_SET, NULL, error))
    {
        do
        {
            if (g_input_stream_read (G_INPUT_STREAM (input_file_stream), &tmp, sizeof (tmp), NULL, error) < 0)
            {
                return FALSE;
            }
            if ((count % 2) == 0)
            {
                skp_version[len] = tmp;
                len++;
            }
            count++;
        }
        while (tmp != '}');
    }

    skp_version[len] = '\0';

    return (skp_version);
}

/* Search thumbnail offset. */
gboolean
search_thumbnail_offset(GFileInputStream *input_file_stream,
                        guint            *png_offset,
                        GError          **error)
{
    /* SketchUP file older than version 21 */
    const guchar thumbnail_signature_1[4] = {0x43, 0x44, 0x69, 0x62};
                                          /* C     D     i     b */
    /* SketchUP file from version 21 */
    const guchar thumbnail_signature_2[4] = {0x6D, 0x65, 0x74, 0x61};
                                          /* m     e     t     a */
    const guint max_repeat = 5;
    guchar tmp_buffer[1024];
    guint n_repeat = 0;
    guint offset_from_sig = 0;
    gssize n_byte_read;
    gsize tot_byte_read = 0, p_pos = 0, cur_position = 0;
    guchar thumbnail_signature[4];
    guchar *skp_version;

    skp_version = get_skp_version(input_file_stream, error);
    if (skp_version[1] == 0x32 && skp_version[2] > 0x30)
    {
        memcpy(thumbnail_signature, thumbnail_signature_2, sizeof(thumbnail_signature_2));
        offset_from_sig = 45;
    }
    else
    {
        memcpy(thumbnail_signature, thumbnail_signature_1, sizeof(thumbnail_signature_1));
        offset_from_sig = 8;
    }

    g_seekable_seek (G_SEEKABLE (input_file_stream), 0, G_SEEK_SET, NULL, error);
    do
    {
        n_repeat += 1;
        n_byte_read = g_input_stream_read (G_INPUT_STREAM (input_file_stream), &tmp_buffer, sizeof (tmp_buffer), NULL, error);
        if (n_byte_read < 0)
        {
            return FALSE;
        }
        for (gsize i = 0; i < (gsize) n_byte_read; i++)
        {
            if (tmp_buffer[i] == thumbnail_signature[p_pos])
            {
                p_pos++;
                if (p_pos == sizeof (thumbnail_signature))
                {
                    cur_position = i;
                    goto do_exit;
                }
            }
            else
            {
                p_pos = 0;
            }
        }
        tot_byte_read += n_byte_read;
        cur_position = 0;
    }
    while (n_byte_read || (n_repeat <= max_repeat));
do_exit:
    *png_offset = (tot_byte_read + cur_position + offset_from_sig + 1);

    return TRUE;
}

/* Try to get a valid PNG Thumbnail. */
gboolean
get_png_thumbnail(GFileInputStream *input_file_stream,
                  guint             png_offset,
                  char            **data,
                  gsize            *data_length,
                  GError          **error)
{
    const guchar png_signature[8] = {0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A};
                                    /* .     P     N     G     .     .     .     . */
    const guchar iend_chunk[4] = {0x49, 0x45, 0x4E, 0x44};
                                 /* I     E     N     D */
    guchar png_header[8], chunk_type[4];
    gchar *chunk_data_length_be;
    guint32 chunk_data_length;
    gsize png_stream_lenght = 0;

    /* File Header:  A PNG file starts with an 8 bytes signature:
     * Check if the file contain the PNG header
     */
    g_seekable_seek (G_SEEKABLE (input_file_stream), png_offset, G_SEEK_SET, NULL, error);
    if (g_input_stream_read (G_INPUT_STREAM (input_file_stream), &png_header, sizeof (png_header), NULL, error) < 0)
    {
        return FALSE;
    }
    if (memcmp (png_signature, png_header, sizeof(png_signature)) != 0)
    {
        return FALSE;
    }

    /* Add the Header length (8 bytes) */
    png_stream_lenght = +8;

    /* Chunks: After the header, comes a series of chunks, each of which conveys certain information about the image.
     * A chunk consists of four parts:
     * 1) Chunk Data Length (4 bytes, big-endian);
     * 2) Chunk Type        (4 bytes);
     * 3) Chunk Data        (Length bytes);
     * 4) Chunk CRC         (4 bytes).
     */
    do
    {
        /* Read the Chunk Data Length (4 bytes) */
        png_stream_lenght += 4;
        chunk_data_length_be = g_malloc (4);
        if (g_input_stream_read (G_INPUT_STREAM (input_file_stream), chunk_data_length_be, 4, NULL, error) < 0)
        {
            return FALSE;
        }
        chunk_data_length = get_guint32 (chunk_data_length_be);
        g_free (chunk_data_length_be);

        /* Read the Chunk Type (4 bytes) */
        png_stream_lenght += 4;
        if (g_input_stream_read (G_INPUT_STREAM (input_file_stream), &chunk_type, 4, NULL, error) < 0)
        {
            return FALSE;
        }

        /* Skip the Chunk Data and CRC (Data Length + 4 bytes) */
        png_stream_lenght += chunk_data_length + 4;
        if (g_input_stream_skip (G_INPUT_STREAM (input_file_stream), chunk_data_length + 4, NULL, error) < 0)
        {
            return FALSE;
        }
    }
    while (memcmp (chunk_type, iend_chunk, sizeof(chunk_type)) != 0);

    /* Read the PNG image data */
    gchar *png_stream = g_malloc (png_stream_lenght);
    g_seekable_seek (G_SEEKABLE (input_file_stream), png_offset, G_SEEK_SET, NULL, error);
    if (g_input_stream_read (G_INPUT_STREAM (input_file_stream), png_stream, png_stream_lenght, NULL, error) < 0)
    {
        return FALSE;
    }

    g_input_stream_close (G_INPUT_STREAM (input_file_stream), NULL, NULL);
    g_object_unref (input_file_stream);

    *data_length = png_stream_lenght;
    *data = png_stream;

    return TRUE;
}

/* Try to get a valid JFIF Thumbnail. */
gboolean
get_jfif_thumbnail(GFileInputStream *input_file_stream,
                   guint             jfif_offset,
                   char            **data,
                   gsize            *data_length,
                   GError          **error)
{
    const guchar jfif_soi_app0[4] = {0xFF, 0xD8, 0xFF, 0xE0};
    const guchar jfif_signature[5] = {0x4A, 0x46, 0x49, 0x46, 0x00};
                                     /* J     F     I     F     . */
    const guchar jfif_eoi[2] = {0xFF, 0xD9};
    const guint max_repeat = 5;

    guchar jfif_header[4], jfif_identifier[5];
    guchar tmp_buffer[1024];
    guint n_repeat = 0;
    gssize n_byte_read;
    gsize jfif_stream_lenght = 0, tot_byte_read = 0, p_pos = 0, cur_position = 0;

    /* File Header:  A JFIF file starts with an 2 bytes SOI and 2 bytes APP0 marker.
     * Check if the file contain the JFIF header
     */
    g_seekable_seek (G_SEEKABLE (input_file_stream), jfif_offset, G_SEEK_SET, NULL, error);
    if (g_input_stream_read (G_INPUT_STREAM (input_file_stream), &jfif_header, sizeof (jfif_header), NULL, error) < 0)
    {
        return FALSE;
    }
    if (memcmp (jfif_soi_app0, jfif_header, sizeof(jfif_soi_app0)) != 0)
    {
        return FALSE;
    }

    /* Skip 2 bytes */
    if (g_input_stream_skip (G_INPUT_STREAM (input_file_stream), 2, NULL, error) < 0)
    {
        return FALSE;
    }

    /* File Identifier:  A JFIF also have an 5 bytes identifier.
     * Check if the file contain the JFIF identifier
     */
    if (g_input_stream_read (G_INPUT_STREAM (input_file_stream), &jfif_identifier, sizeof (jfif_identifier), NULL, error) < 0)
    {
        return FALSE;
    }
    if (memcmp (jfif_signature, jfif_identifier, sizeof(jfif_signature)) != 0)
    {
        return FALSE;
    }

    /* Add the 4 bytes SOI and APP0, the 2 bytes skipped and the 5 bytes Identifier */
    jfif_stream_lenght = 4 + 2 + 5;

    /* Search the JFIF end file marker */
    do
    {
        n_repeat += 1;
        n_byte_read = g_input_stream_read (G_INPUT_STREAM (input_file_stream), &tmp_buffer, sizeof (tmp_buffer), NULL, error);
        if (n_byte_read < 0)
        {
            return FALSE;
        }
        for (gsize i = 0; i < (gsize) n_byte_read; i++)
        {
            if (tmp_buffer[i] == jfif_eoi[p_pos])
            {
                p_pos++;
                if (p_pos == sizeof (jfif_eoi))
                {
                    cur_position = i;
                    goto do_exit;
                }
            }
            else
            {
                p_pos = 0;
            }
        }
        tot_byte_read += n_byte_read;
        cur_position = 0;
    }
    while (n_byte_read || (n_repeat <= max_repeat));
do_exit:
    /* Add total byte read with do iteration*/
    jfif_stream_lenght += (tot_byte_read + cur_position + 1);

    /* Read the JFIF image data */
    gchar *jfif_stream = g_malloc (jfif_stream_lenght);
    g_seekable_seek (G_SEEKABLE (input_file_stream), jfif_offset, G_SEEK_SET, NULL, error);
    if (g_input_stream_read (G_INPUT_STREAM (input_file_stream), jfif_stream, jfif_stream_lenght, NULL, error) < 0)
    {
        return FALSE;
    }

    g_input_stream_close (G_INPUT_STREAM (input_file_stream), NULL, NULL);
    g_object_unref (input_file_stream);

    *data_length = jfif_stream_lenght;
    *data = jfif_stream;

    return TRUE;
}

char *
file_to_data (const char *path,
              gsize      *ret_length,
              GError    **error)
{
    GFile *input_file;
    GFileInputStream *input_file_stream;
    guint img_offset;
    char *data = NULL;
    gsize data_length;

    /* Open the skp file for reading */
    input_file = g_file_new_for_path (path);
    input_file_stream = g_file_read (input_file, NULL, error);
    g_object_unref (input_file);

    if (input_file_stream == NULL)
    {
        return NULL;
    }

    /* Check if the opened skp file is seekable */
    if (g_seekable_can_seek (G_SEEKABLE (input_file_stream)) != TRUE)
    {
        g_warning ("File is not seekable");
        return NULL;
    }

    /* Check if the opened skp file is valid */
    if (is_skpfile (input_file_stream, error) != TRUE)
    {
        return NULL;
    }

    if (search_thumbnail_offset (input_file_stream, &img_offset, error) == TRUE)
    {
        /* Try to extract PNG thumbnail, almost case */
        if (get_png_thumbnail (input_file_stream, img_offset, &data, &data_length, error) == TRUE)
        {
            *ret_length = data_length;
        }
        /* Try to extract JFIF thumbnail, some case */
        else if (get_jfif_thumbnail (input_file_stream, img_offset, &data, &data_length, error) == TRUE)
        {
            *ret_length = data_length;
        }
    }
    else
    {
        g_input_stream_close (G_INPUT_STREAM (input_file_stream), NULL, NULL);
        g_object_unref (input_file_stream);
    }
    return data;
}

